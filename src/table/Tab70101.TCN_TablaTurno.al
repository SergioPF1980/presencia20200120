table 70101 "TCN_TablaTurno"
{
    Caption = 'Tabla Turno';
    LookupPageId = "TCN_PaginaListaTurno";
    DataClassification = ToBeClassified;
    DrillDownPageId = "TCN_PaginaListaTurno";

    fields
    {
        field(01; Codigo; Code[10])
        {
            Caption = 'Codigo';
            DataClassification = ToBeClassified;
        }
        field(02; Descripcion; Text[50])
        {
            Caption = 'Descripcion';
            DataClassification = ToBeClassified;
        }
        field(03; "MinutosDescanodesayuno"; Integer)
        {
            Caption = 'Minutos descano/desayuno';
            DataClassification = ToBeClassified;
        }
        field(04; "UsuarioAlta"; Code[50])
        {
            Caption = 'Usuario Alta';
            DataClassification = ToBeClassified;
            Editable = false;
        }

        field(05; "UsuarioModificacion"; Code[50])
        {
            Caption = 'Usuario Modificación';
            DataClassification = ToBeClassified;
            Editable = false;
        }

        field(06; "FechaAlta"; DateTime)
        {
            Caption = 'Fecha y hora de Alta';
            DataClassification = ToBeClassified;
            Editable = false;
        }
        field(07; "FechaModificacion"; DateTime)
        {
            Caption = 'Fecha y hora de Modificación';
            DataClassification = ToBeClassified;
            Editable = false;
        }

        field(08; "NumEmpleadoAsig"; Integer)
        {
            Caption = 'Nº Empleado Asignados';
            FieldClass = FlowField;
            CalcFormula = count (Employee where (TCN_CodTurno = field (Codigo)));
            Editable = false;
        }


    }
    keys
    {
        key(PK; Codigo)
        {
            Clustered = true;
        }
    }
    //Los Fieldgroup son para ver en Avanzado Codigo y Descripcion
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Descripcion)
        {

        }
        fieldgroup(Brick; Codigo, Descripcion)
        {

        }

    }

    trigger OnInsert()
    begin
        UsuarioAlta := UserId;
        FechaAlta := CurrentDateTime;
    end;

    trigger OnModify()

    begin
        UsuarioModificacion := UserId;
        FechaModificacion := CurrentDateTime;
    end;

    trigger OnDelete()
    var
    //clError: Label 'Existe una Ausencvia Registrada con esta ausencia %1, imposible borrar';
    begin
        //no borrar turnos con empleados asignados
        CalcFields(NumEmpleadoAsig);
        // if NumEmpleadoAsig > 0 then begin
        //     Error(clError, NumEmpleadoAsig);
        // end;
        TestField(NumEmpleadoAsig, 0);

    end;


}
