table 70100 "TCN_TablaConfPresencia"
{
    Caption = 'Configuración Presencia';
    DataClassification = ToBeClassified;

    fields
    {
        field(01; ID; Code[10])
        {
            Caption = 'ID';
            DataClassification = ToBeClassified;
        }
        field(02; "Minutos de cortesia entrada"; Text[10])
        {
            Caption = 'Minutos de cortesia entrada';
            DataClassification = ToBeClassified;
            CharAllowed = '+-0123456789';
        }
        field(03; "Minutos de cortesia salida"; Text[10])
        {
            Caption = 'Minutos de cortesia salida';
            DataClassification = ToBeClassified;
            CharAllowed = '+-0123456789';   //caracteres permitidos
        }
        field(04; "Minutos entre fichajes"; Integer)
        {
            Caption = 'Minutos entre fichajes';
            DataClassification = ToBeClassified;    //caracteres permitidos
        }

        field(05; IdUnico; BigInteger)
        {
            Caption = 'Id Unico';
            DataClassification = ToBeClassified;
        }

        /* Dias Semana */
        field(06; Lunes; Boolean)
        {
            Caption = 'Lunes';
            DataClassification = ToBeClassified;
        }
        field(07; Martes; Boolean)
        {
            Caption = 'Martes';
            DataClassification = ToBeClassified;
        }
        field(08; Miercoles; Boolean)
        {
            Caption = 'Miercoles';
            DataClassification = ToBeClassified;
        }
        field(09; Jueves; Boolean)
        {
            Caption = 'Jueves';
            DataClassification = ToBeClassified;
        }
        field(10; Viernes; Boolean)
        {
            Caption = 'Viernes';
            DataClassification = ToBeClassified;
        }
        field(11; Sabado; Boolean)
        {
            Caption = 'Sabado';
            DataClassification = ToBeClassified;
        }
        field(12; Domingo; Boolean)
        {
            Caption = 'Domingo';
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PK; ID)
        {
            Clustered = true;
        }
    }

    procedure getF() xSalida: Boolean

    begin
        if not get() then begin
            Init();
            Insert(true);

        end;
        xSalida := true;
    end;

    procedure getIdUnicoF() xSalida: Biginteger
    var

    begin

        getF(); //recojo los valores
        LockTable(true);
        IdUnico += 1;
        Validate(IdUnico, IdUnico + 1);
        xSalida := IdUnico;
        Modify(true);   // graba el registro
    end;


}
