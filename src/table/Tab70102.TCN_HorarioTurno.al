table 70102 "TCN_HorarioTurno"
{
    Caption = 'Horario Turno';
    DataClassification = ToBeClassified;
    LookupPageId = TCN_ListaHorarioTurno;
    DrillDownPageId = TCN_ListaHorarioTurno;

    fields
    {
        field(01; TCN_CodigoTurno; Code[10])
        {
            Caption = 'Codigo Turno';
            DataClassification = ToBeClassified;
            TableRelation = TCN_TablaTurno.Codigo;   //Enlazamos la clave de Tabla Turno
        }
        field(02; TCN_HoraInicio; Time)
        {
            Caption = 'Hora Inicio';
            DataClassification = ToBeClassified;
        }
        field(3; TCN_HoraFinal; Time)
        {
            Caption = 'Hora Final';
            DataClassification = ToBeClassified;
        }
        field(4; NumTurnos; Integer)
        {
            Caption = 'Nº Turnos';
            FieldClass = FlowField;
            CalcFormula = count (TCN_TablaTurno);
        }

    }
    keys
    {
        key(PK; TCN_CodigoTurno)
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
        fieldgroup(DropDown; TCN_CodigoTurno, TCN_HoraInicio, TCN_HoraFinal)
        {

        }
    }

}
