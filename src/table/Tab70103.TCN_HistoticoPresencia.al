table 70103 "TCN_HistoticoPresencia"
{
    Caption = 'Histotico Presencia';
    DataClassification = ToBeClassified;

    fields
    {
        field(01; TCN_NumeroOrden; BigInteger)
        {
            //AutoIncrement = true;
            Caption = 'Nº Orden';
            DataClassification = ToBeClassified;
            Editable = false;

        }

        field(02; TCN_NumeroEmpleado; Code[10])
        {
            Caption = 'Numero Empleado';
            DataClassification = ToBeClassified;
            TableRelation = Employee."No."; //tabla del sistema

            trigger OnValidate()
            var
                rlEmployee: Record Employee;
            begin

                if rlEmployee.Get(TCN_NumeroEmpleado) then begin
                    Validate(TCN_CodTurno, rlEmployee.TCN_CodTurno);
                end else begin
                    Validate(TCN_CodTurno, '');
                end;

            end;
        }
        field(03; TCN_TipoMovimiento; Enum TCN_TipoFichaje)
        {
            Caption = 'Tipo Movimiento';
            DataClassification = ToBeClassified;
            InitValue = 'Presencia';

            trigger OnValidate()

            begin
                // if TCN_NumeroOrden = 0 then begin
                // end else begin
                //     Modify(true);
                // end;
                CalcularDifHorasF();
            end;

        }
        field(04; TCN_CodAusencia; Code[10])
        {
            TableRelation = "Cause of Absence".Code;
            Caption = 'Cod Ausencia';
            DataClassification = ToBeClassified;
            // trigger OnValidate()
            // begin
            //     if TCN_CodAusencia = '' then begin
            //         Validate(TCN_TipoMovimiento, TCN_TipoMovimiento::Presencia);
            //     end else begin
            //         Validate(TCN_TipoMovimiento, TCN_TipoMovimiento::Ausencia);
            //     end;
            // end;
        }
        field(05; TCN_DescAusencia; Text[250])
        {
            Caption = 'Descripción Ausencia';
            FieldClass = FlowField;
            CalcFormula = lookup ("Cause of Absence".Description where (code = field (TCN_CodAusencia)));
            Editable = false;
        }
        field(06; TCN_Inicio; DateTime)
        {
            Caption = 'Inicio';
            DataClassification = ToBeClassified;

            trigger OnValidate()

            begin
                // Validate(TCN_FechaInicio, DT2Date(TCN_Inicio));
                // validate(TCN_HoraInicio, DT2Time(TCN_Inicio));

                if CalcularFechaHoraF(TCN_Inicio, TCN_FechaInicio, TCN_HoraInicio) then begin
                    Validate(TCN_FechaInicio);
                    Validate(TCN_HoraInicio);
                end else begin
                    Error('Formato %1 incorrecto', FieldCaption(TCN_Inicio));
                end;
                CalcularDifHorasF();
            end;

        }
        field(07; TCN_FechaInicio; Date)
        {
            Caption = 'Fecha Inicio';
            DataClassification = ToBeClassified;
            Editable = false;
        }
        field(08; TCN_HoraInicio; Time)
        {
            Caption = 'Hora Inicio';
            DataClassification = ToBeClassified;
            Editable = false;


        }
        field(09; TCN_Fin; DateTime)
        {
            Caption = 'Fin';
            DataClassification = ToBeClassified;
            trigger OnValidate()
            begin
                Validate(TCN_FechaFin, DT2Date(TCN_Fin));
                Validate(TCN_HoraFin, DT2Time(TCN_Fin));
                CalcularDifHorasF();
            end;

        }
        field(10; TCN_FechaFin; Date)
        {
            Caption = 'Fecha Fin';
            DataClassification = ToBeClassified;
            Editable = false;
        }
        field(11; TCN_HoraFin; Time)
        {
            Caption = 'Hora Fin';
            DataClassification = ToBeClassified;
            Editable = false;
        }
        field(12; TCN_CodTurno; Code[10])
        {
            Caption = 'Cod Turno';
            DataClassification = ToBeClassified;
            TableRelation = TCN_HorarioTurno.TCN_CodigoTurno;



        }
        field(13; TCN_NumeroHoras; Decimal)
        {
            Caption = 'Nº Horas';
            DataClassification = ToBeClassified;
            Editable = false;
        }

    }
    keys
    {
        key(PK; TCN_NumeroOrden)
        {
            Clustered = true;
        }
    }

    trigger OnInsert()
    var
        rlTablaConfPresencia: Record TCN_TablaConfPresencia;
    begin
        //Autoincremental en NumeroOrden 
        if TCN_NumeroOrden = 0 then begin   //compruebo el orden para que me devuelva el orden de la tabla TablaConfPresencia
            Validate(TCN_NumeroOrden, rlTablaConfPresencia.getIdUnicoF());  //Llamamos a la funcion getIdUnicoF()
        end;
    end;

    local procedure CalcularDifHorasF()
    var
        xlDateTimeVacio: DateTime;
        xSalida: decimal;
    begin
        OnBeforeCalcularHorasF(rec, xRec);

        // if not (TCN_Inicio <> xlDateTimeVacio) and (TCN_Fin <> xlDateTimeVacio) then begin
        //     if (TCN_Fin > TCN_Inicio) then begin
        //         xSalida := Round((TCN_Fin - TCN_Inicio) / 3600000, 0.01);
        //     end;
        // end;

        // Validate(TCN_NumeroHoras, xSalida);

        if (TCN_Fin <> 0DT) and (TCN_Fin < TCN_Inicio) then begin
            Error('Fin no puede tener hora anterior a inicio');
        end;
        if (TCN_Fin <> 0DT) and (TCN_Inicio <> 0DT) then begin

            Validate(TCN_NumeroHoras, Round((tcn_fin - TCN_Inicio) / 3600000, 0.01));
        end else begin
            Validate(TCN_NumeroHoras, 0);
        end;
        OnAfterCalcularHorasF(rec, xRec);

    end;

    [IntegrationEvent(false, false)]
    local procedure OnBeforeCalcularHorasF(var prRec: Record TCN_HistoticoPresencia; pxrRec: Record TCN_HistoticoPresencia)
    begin
        //No puede haber código
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterCalcularHorasF(var prRec: Record TCN_HistoticoPresencia; pxrRec: Record TCN_HistoticoPresencia)
    begin
        //No puede haber código
    end;


    /* Solucion Jose*/
    local procedure CalcularFechaHoraF(pDateTime: DateTime; var pFecha: Date; var pHora: Time) xSalida: Boolean
    begin
        if pDateTime <> 0DT then begin
            pFecha := DT2Date(pDateTime);
            pHora := DT2Time(pDateTime);
            xSalida := true;
        end;
    end;

}


