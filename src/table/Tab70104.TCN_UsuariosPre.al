table 70104 "TCN_UsuariosPre"
{
    DataClassification = ToBeClassified;
    LookupPageId = TCN_UsuariosPresencia;
    DrillDownPageId = TCN_UsuariosPresencia;

    fields
    {
        field(1; "IdUsuario"; Code[50])
        {
            Caption = 'Id Usuario';
            TableRelation = "User Setup"."User ID";
            DataClassification = ToBeClassified;
        }
        field(2; "SuperPresencia"; Boolean)
        {
            Caption = 'Super Presencia';
            DataClassification = ToBeClassified;
        }

    }

    keys
    {
        key(PK; IdUsuario)
        {
            Clustered = true;
        }
    }
    // funcion obtener el nombre
    procedure NombreUsuarioF(pCodNombre: code[50]) xSalida: Text
    var
        rlUser: Record User;
    begin
        rlUser.SetRange("User Name", pCodNombre);   //utilizamos el range porque no es clave primaria
        if rlUser.FindFirst() then begin
            xSalida := rlUser."Full Name";
        end;
    end;





}