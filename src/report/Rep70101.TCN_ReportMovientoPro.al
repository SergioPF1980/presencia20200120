report 70101 "TCN_ReportMovientoPro"
{
    Caption = 'Informe';
    UsageCategory = Administration;
    ApplicationArea = all;

    DefaultLayout = RDLC;
    RDLCLayout = 'ListadoMovimientoProducto.rdl';
    PreviewMode = PrintLayout;

    dataset
    {
        dataitem("Movimiento Procucto"; "Item Ledger Entry")
        {
            CalcFields = "Sales Amount (Actual)", "Purchase Amount (Actual)";
            DataItemTableView = where ("Source Type" = filter (Customer | Vendor));

            column(MostrarDetalle; xMostrarDetalle)
            {
            }
            column(DocumentDate; "Document Date")
            {
            }
            column(DocumentNo; "Document No.")
            {
            }
            column(SourceType; "Source Type")
            {
            }
            column(ItemNo; "Item No.")
            {
            }
            column(SalesAmountActual; "Sales Amount (Actual)")
            {
            }
            column(PurchaseAmountActual; "Purchase Amount (Actual)")
            {
            }
            column(SourceNo; "Source No.")
            {
            }

            dataitem("Cliente"; Customer)
            {

                DataItemLink = "No." = field ("Source No.");
                column(Address; Address)
                {
                }
                column(Name; Name)
                {
                }
            }

            dataitem("Procucto"; Item)
            {
                DataItemLink = "No." = field ("Item No.");
                column(Description; Description)
                {
                }
                column(ItemCategoryCode; "Item Category Code")
                {
                }
            }

            // trigger OnAfterGetRecord()
            // begin
            //     CalcFields("Sales Amount (Actual)");
            // end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(xMostrarDetalle; xMostrarDetalle)
                    {

                    }
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }
    var
        xMostrarDetalle: Boolean;
}