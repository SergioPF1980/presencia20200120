report 70103 "TCN_EtiquetaProducto"
{
    Caption = 'ReportName';
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'EtiquetaProd.rdl';
    UseRequestPage = false;

    dataset
    {
        dataitem(Item; Item)
        {
            // RequestFilterFields = "No.";
            column(No_; "No.")
            {

            }
            dataitem(Integer; Integer)
            {
                column(myInt; myInt) { }

                trigger OnPreDataItem()
                begin
                    Integer.SetRange(Number, 1, xCopias);
                end;

                trigger OnAfterGetRecord()
                begin
                    myInt += 1;
                end;
            }
            trigger OnAfterGetRecord()
            begin
                // xCopias := "Sales Line".Quantity;
            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                // group(GroupName)
                // {
                //     field(Name; SourceExpression)
                //     {
                //         ApplicationArea = All;

                //     }
                // }
            }
        }

    }
    var
        xCopias: Integer;
        myInt: Integer;

    procedure SetDatosF(pItem: Record Item; pCopias: Integer)
    begin
        item.SetRange("No.", pItem."No.");
        xCopias := pCopias;
    end;
}