report 70104 "TCN_ListadoProductoWord"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    WordLayout = 'ListadoProductoWord.docx';
    DefaultLayout = Word;

    // DefaultLayout = RDLC;
    // RDLCLayout = 'EtiquetaProd.rdl';

    dataset
    {
        dataitem(Item; Item)
        {
            // CalcFields = Picture;
            column(No_; "No.") { }
            column(Description; Description) { }
            column(Picture; Picture) { }

            dataitem("Item Ledger Entry"; "Item Ledger Entry")
            {
                DataItemLink = "Item No." = field ("No.");
                column(Source_No_; "Source No.") { }

                trigger OnAfterGetRecord()
                var

                begin
                    if "Source No." = '' then
                        CurrReport.Skip();  //Skip salta
                end;
            }
            trigger OnAfterGetRecord()
            var

            begin
                rItemLedgerEntry.SetRange("Item No.", "No.");
                rItemLedgerEntry.SetFilter("Source No.", '<>%1', '');
                if rItemLedgerEntry.IsEmpty then
                    CurrReport.Skip();  //Skip salta
            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    var
        myInt: Integer;
        rItemLedgerEntry: Record "Item Ledger Entry";
}