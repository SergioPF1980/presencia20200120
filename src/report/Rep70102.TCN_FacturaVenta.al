report 70102 "TCN_FacturaVenta"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    RDLCLayout = 'FacturaVenta.rdl';
    DefaultLayout = RDLC;
    PreviewMode = PrintLayout;

    dataset
    {
        dataitem(SalesInvoiceHeader; "Sales Invoice Header")
        {
            RequestFilterFields = "No.";
            column(No; "No.")
            {
            }
            column(PostingDate; "Posting Date")
            {
            }
            column(SelltoCustomerName; "Sell-to Customer Name")
            {
            }
            column(NumCopias; xInt) { }
            column(Label1; xLabel1)
            {
                CaptionML = ESP = 'Nº Documneto', ENG = 'Document Nº';
            }
            column(Logo; rCompanyInformation.Picture) { }

            column(xCusAddre1; xCusAddre[1]) { }
            column(xCusAddre2; xCusAddre[2]) { }
            column(xCusAddre3; xCusAddre[3]) { }
            column(xCusAddre4; xCusAddre[4]) { }
            column(xCusAddre5; xCusAddre[5]) { }
            column(xCusAddre6; xCusAddre[6]) { }
            column(xCusAddre7; xCusAddre[7]) { }
            column(xCusAddre8; xCusAddre[8]) { }

            column(xCompanyAddre1; xCompanyAddre[1]) { }
            column(xCompanyAddre2; xCompanyAddre[2]) { }
            column(xCompanyAddre3; xCompanyAddre[3]) { }
            column(xCompanyAddre4; xCompanyAddre[4]) { }
            column(xCompanyAddre5; xCompanyAddre[5]) { }
            column(xCompanyAddre6; xCompanyAddre[6]) { }
            column(xCompanyAddre7; xCompanyAddre[7]) { }
            column(xCompanyAddre8; xCompanyAddre[8]) { }

            column(xVATAmount11; xVATAmount[1, 1]) { }
            column(xVATAmount21; xVATAmount[2, 1]) { }
            column(xVATAmount31; xVATAmount[3, 1]) { }
            column(xVATAmount41; xVATAmount[4, 1]) { }

            column(xVATAmount12; xVATAmount[1, 2]) { }
            column(xVATAmount22; xVATAmount[2, 2]) { }
            column(xVATAmount32; xVATAmount[3, 2]) { }
            column(xVATAmount42; xVATAmount[4, 2]) { }

            column(xVATAmount13; xVATAmount[1, 3]) { }
            column(xVATAmount23; xVATAmount[2, 3]) { }
            column(xVATAmount33; xVATAmount[3, 3]) { }
            column(xVATAmount43; xVATAmount[4, 3]) { }

            column(xVATAmount14; xVATAmount[1, 4]) { }
            column(xVATAmount24; xVATAmount[2, 4]) { }
            column(xVATAmount34; xVATAmount[3, 4]) { }
            column(xVATAmount44; xVATAmount[4, 4]) { }

            dataitem(Copias; Integer)
            {

                dataitem("Sales Invoice Line"; "Sales Invoice Line")
                {
                    DataItemLinkReference = SalesInvoiceHeader;
                    DataItemLink = "Document No." = field ("No.");

                    column(Line_No_; "Line No.") { }
                    column(Description; Description)
                    {
                    }
                    column(LineAmount; "Line Amount")
                    {
                    }
                    column(ItemNo; "No.")
                    {
                    }
                    column(LineDiscount; "Line Discount %")
                    {
                    }
                    column(Quantity; Quantity)
                    {
                    }
                    column(UnitPrice; "Unit Price")
                    {
                    }
                    column(xMostrarRefCruzada; xMostrarRefCruzada)
                    {
                    }
                    column(rlItemCrossReferenceNo; rlItemCrossReference."Cross-Reference No.")
                    {
                    }
                    column(rlItemCrossReferenceDesc; rlItemCrossReference.Description)
                    {
                    }
                    dataitem(TMPTrakingSpecification; "Tracking Specification")
                    {
                        UseTemporary = true;
                        DataItemLink = "Source Ref. No." = field ("Line No."), "Source ID" = field ("Document No.");
                        column(Lot_No; "Lot No.") { }
                        column(Serial_No_; "Serial No.") { }
                        column(Quantity__Base_; "Quantity (Base)") { }
                    }
                    trigger OnAfterGetRecord()
                    begin
                        xMostrarRefCruzada := ObtenerReferenciaCruzadaF();
                        Clear(TMPTrakingSpecification);
                        TMPTrakingSpecification.DeleteAll();
                        SeguimientoProductosF();
                    end;

                }
                trigger OnPreDataItem()

                var

                begin
                    Copias.SetRange(Number, 0, xCopias);
                end;

                trigger OnAfterGetRecord()
                begin
                    xInt += 1;
                end;


            }

            trigger OnAfterGetRecord()
            var
                rlCustomer: Record Customer;
                rlLanguage: Record Language;
            begin
                rlCustomer.get(SalesInvoiceHeader."Sell-to Customer No.");
                CurrReport.Language(rlLanguage.GetLanguageID(rlCustomer."Language Code"));

                ObtenerDireccionesF();
                xInt := 0;

                RellenarIVAF(SalesInvoiceHeader);
            end;
        }

    }


    requestpage
    {
        SaveValues = True;
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field("Numero Copias"; xCopias)
                    {
                        ApplicationArea = All;
                        Caption = 'Número de Copias';
                    }
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }
    var
        xCusAddre: array[8] of Text;
        xShipAddre: array[8] of Text;
        xCompanyAddre: array[8] of Text;
        cuFormatAddress: Codeunit "Format Address";

        xVATAmount: array[4, 4] of Decimal;
        xCopias: Integer;
        xInt: Integer;

        rCompanyInformation: Record "Company Information";
        rResponsibilityCenter: Record "Responsibility Center";
        rTempVATAmountLine: Record "VAT Amount Line" temporary;

        xLabel1: Label 'Prueba';

        xMostrarRefCruzada: Boolean;
        rlItemCrossReference: Record "Item Cross Reference";

        /*------------------------Procedures---------------------------------*/
    local procedure MostrarDireccionEnvioF(): Boolean
    var
        i: Integer;
    begin
        for i := 0 to 8 do begin
            // if xCusAddre[i] <> xShipAddre[i] then begin
            //     Exit(true);
            // end;
            if xCusAddre[i] <> xShipAddre[i] then begin
                xCusAddre[i] := xShipAddre[1];
            end;
        end;
    end;

    local procedure ObtenerDireccionesF()
    begin
        cuFormatAddress.GetCompanyAddr(SalesInvoiceHeader."Responsibility Center", rResponsibilityCenter, rCompanyInformation, xCompanyAddre);
        if xCompanyAddre[1] = '' then begin
            //cuFormatAddress.Company(xCompanyAddre, rCompanyInformation);
            xCompanyAddre[1] := rCompanyInformation.Name;
            xCompanyAddre[2] := rCompanyInformation.Address;
            xCompanyAddre[3] := rCompanyInformation."Address 2";
            xCompanyAddre[4] := rCompanyInformation."Post Code";
            xCompanyAddre[5] := rCompanyInformation."Country/Region Code";
            xCompanyAddre[6] := rCompanyInformation."Phone No.";
            xCompanyAddre[7] := rCompanyInformation."E-Mail";
            xCompanyAddre[8] := rCompanyInformation."Home Page";
        end;
        cuFormatAddress.SalesInvSellTo(xCusAddre, SalesInvoiceHeader);
        cuFormatAddress.SalesInvShipTo(xShipAddre, xCusAddre, SalesInvoiceHeader);
    end;

    local procedure RellenarIVAF(var prSalesInvoiceHeader: Record "Sales Invoice Header")
    var
        rlSalesInvoiceLine: Record "Sales Invoice Line";
        i: Integer;
    begin
        Clear(rTempVATAmountLine);
        Clear(xVATAmount);
        i := 0;
        rlSalesInvoiceLine.SetRange("Document No.", prSalesInvoiceHeader."No.");
        if rlSalesInvoiceLine.FindSet() then
            repeat
                rTempVATAmountLine.SetRange("VAT Identifier", rlSalesInvoiceLine."VAT Identifier");
                if rTempVATAmountLine.FindFirst() then begin

                    rTempVATAmountLine."VAT Base" += rlSalesInvoiceLine.Amount;
                    rTempVATAmountLine."VAT Amount" += rlSalesInvoiceLine."Amount Including VAT" - rlSalesInvoiceLine.Amount;
                    rTempVATAmountLine."Amount Including VAT" += rlSalesInvoiceLine."Amount Including VAT";
                    rTempVATAmountLine.Modify(false);
                end else begin
                    // rTempVATAmountLine.Init();
                    rTempVATAmountLine."VAT Identifier" := rlSalesInvoiceLine."VAT Identifier";
                    rTempVATAmountLine."VAT Calculation Type" := rlSalesInvoiceLine."VAT Calculation Type";
                    rTempVATAmountLine."VAT %" := rlSalesInvoiceLine."VAT %";
                    rTempVATAmountLine."VAT Base" := rlSalesInvoiceLine.Amount;
                    rTempVATAmountLine."VAT Amount" := rlSalesInvoiceLine."Amount Including VAT" - rlSalesInvoiceLine.Amount;
                    rTempVATAmountLine."Amount Including VAT" := rlSalesInvoiceLine."Amount Including VAT";
                    rTempVATAmountLine.Insert();
                end;

            until rlSalesInvoiceLine.Next() = 0;

        if rTempVATAmountLine.FindSet() then
            repeat
                i += 1;
                if i > 4 then
                    Error('Solo se permiten 4 tipos de IVA, si necesita más avise');
                xVATAmount[1, i] := rTempVATAmountLine."VAT %";
                xVATAmount[2, i] := rTempVATAmountLine."VAT Base";
                xVATAmount[3, i] := rTempVATAmountLine."VAT Amount";
                xVATAmount[4, i] := rTempVATAmountLine."Amount Including VAT";

            until rTempVATAmountLine.Next() = 0;
    end;

    local procedure ObtenerReferenciaCruzadaF(): Boolean
    var

    begin
        rlItemCrossReference.SetRange("Item No.", "Sales Invoice Line"."No.");
        rlItemCrossReference.SetRange("Variant Code", "Sales Invoice Line"."Variant Code");
        rlItemCrossReference.SetRange("Unit of Measure", "Sales Invoice Line"."Unit of Measure Code");
        rlItemCrossReference.SetRange("Cross-Reference Type", "Sales Invoice Line"."Cross-Reference Type");
        rlItemCrossReference.SetRange("Cross-Reference Type No.", "Sales Invoice Line"."Cross-Reference Type No.");
        rlItemCrossReference.SetRange("Cross-Reference No.", "Sales Invoice Line"."Cross-Reference No.");

        if not rlItemCrossReference.FindFirst() then begin
            rlItemCrossReference.SetRange("Cross-Reference No.");
            rlItemCrossReference.SetRange("Variant Code");
            rlItemCrossReference.SetRange("Unit of Measure");
            rlItemCrossReference.SetRange("Cross-Reference Type", rlItemCrossReference."Cross-Reference Type"::Customer);
            rlItemCrossReference.SetRange("Cross-Reference Type No.", "Sales Invoice Line"."Sell-to Customer No.");

            if not rlItemCrossReference.FindFirst() then begin
                rlItemCrossReference.SetRange("Cross-Reference Type");
                rlItemCrossReference.SetRange("Cross-Reference Type No.");
                if not rlItemCrossReference.FindFirst() then begin
                    Clear(rlItemCrossReference);
                end;
            end;
        end;
        if rlItemCrossReference."Cross-Reference No." <> '' then begin
            exit(false);
        end else begin
            exit(true);
        end;
    end;


    local procedure SeguimientoProductosF()
    var
        rlTrackingSpecification: Record "Tracking Specification";
        rlReservationEntry: Record "Reservation Entry";
        rlItemLedgerEntry: Record "Item Ledger Entry";
        rlValueEntry: Record "Value Entry";
        myInteger: Integer;
    begin
        rlTrackingSpecification.SetRange("Source ID", SalesInvoiceHeader."No.");
        rlTrackingSpecification.SetRange("Source Ref. No.", "Sales Invoice Line"."Line No.");

        TMPTrakingSpecification.Init();
        if rlTrackingSpecification.FindFirst() then begin
            repeat
                myInteger += 1;
                TMPTrakingSpecification := rlTrackingSpecification;
                TMPTrakingSpecification.Insert();
            until
            rlTrackingSpecification.Next() = 0;

        end else
            if rlTrackingSpecification.IsEmpty then begin
                rlReservationEntry.SetRange("Source ID", SalesInvoiceHeader."No.");
                rlReservationEntry.SetRange("Source Ref. No.", "Sales Invoice Line"."Line No.");
                if rlReservationEntry.FindSet() then begin
                    repeat
                        myInteger += 1;
                        TMPTrakingSpecification."Entry No." := rlReservationEntry."Entry No.";
                        TMPTrakingSpecification."Source ID" := rlReservationEntry."Source ID";
                        TMPTrakingSpecification."Source Ref. No." := rlReservationEntry."Source Ref. No.";
                        TMPTrakingSpecification."Item No." := rlReservationEntry."Item No.";
                        TMPTrakingSpecification."Lot No." := rlReservationEntry."Lot No.";
                        TMPTrakingSpecification."Serial No." := rlReservationEntry."Serial No.";
                        TMPTrakingSpecification."Quantity (Base)" := rlReservationEntry."Quantity (Base)";
                        TMPTrakingSpecification.Insert(false);

                    until rlItemLedgerEntry.Next() = 0;
                end else begin
                    rlValueEntry.SetRange("Document No.", SalesInvoiceHeader."No.");
                    rlValueEntry.SetRange("Document Line No.", "Sales Invoice Line"."Line No.");

                    if rlValueEntry.FindSet() then begin
                        repeat
                            myInteger += 1;
                            if rlItemLedgerEntry.Get(rlValueEntry."Item Ledger Entry No.") then begin
                                TMPTrakingSpecification."Entry No." := rlItemLedgerEntry."Entry No.";
                                TMPTrakingSpecification."Source ID" := rlValueEntry."Document No.";
                                TMPTrakingSpecification."Source Ref. No." := rlItemLedgerEntry."Document Line No.";
                                TMPTrakingSpecification."Item No." := rlItemLedgerEntry."Item No.";
                                TMPTrakingSpecification."Lot No." := rlItemLedgerEntry."Lot No.";
                                TMPTrakingSpecification."Serial No." := rlItemLedgerEntry."Serial No.";
                                TMPTrakingSpecification."Quantity (Base)" := rlItemLedgerEntry."Quantity";
                                TMPTrakingSpecification.Insert(false);
                            end;
                        until rlValueEntry.Next() = 0;
                    end;
                end;
            end;
    end;
    /*------------------------Triger---------------------------------*/
    trigger OnPreReport()
    begin
        rCompanyInformation.Get();
        rCompanyInformation.CalcFields(Picture);
    end;


}