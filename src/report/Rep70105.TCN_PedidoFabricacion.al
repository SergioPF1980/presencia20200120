report 70105 "TCN_PedidoFabricacion"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'PedidoFabricacion.rdl';
    PreviewMode = PrintLayout;

    dataset
    {
        dataitem("Production Order"; "Production Order")
        {
            RequestFilterFields = "No.";
            column(xComentarios; xComentarios)
            {

            }
            column(No_; "No.") { }
            column(Description; Description) { }
            column(Cost_Amount; "Cost Amount") { }
            column(Starting_Date; "Starting Date") { }
            column(Ending_Date; "Ending Date") { }
            column(Status; Status) { }

            dataitem("Prod. Order Line"; "Prod. Order Line")
            {
                DataItemLink = "Prod. Order No." = field ("No."), Status = field (Status);

                column(Prod__Order_No_; "Prod. Order No.") { }
                column(Item_No_; "Item No.") { }
                column(Cost_AmountLine; "Cost Amount") { }
                column(DescriptionLine; Description) { }
                column(Line_No_; "Line No.") { }
                column(Quantity; Quantity) { }

                dataitem("Prod. Order Routing Line"; "Prod. Order Routing Line")
                {
                    DataItemLink = Status = field (Status), "Prod. Order No." = field ("Prod. Order No."),
                    "Routing Reference No." = field ("Routing Reference No."), "Routing No." = field ("Routing No.");
                    column(Ending_Date_Time; "Ending Date-Time")
                    {
                    }

                    column(StartingDateTime; "Starting Date-Time")
                    {
                    }
                    column(NoRute; "No.")
                    {
                    }
                    column(DescriptionRute; Description)
                    {
                    }
                    column(OperationNo; "Operation No.")
                    {
                    }
                    column(TypeRute; Type)
                    {
                    }

                }
                dataitem("Prod. Order Component"; "Prod. Order Component")
                {

                    DataItemLink = Status = field (Status), "Prod. Order No." = field ("Prod. Order No."),
                    "Prod. Order Line No." = field ("Line No.");

                    column(DescriptionOrdComp; Description)
                    {

                    }
                    column(ItemNoOrdComp; "Item No.")
                    {

                    }
                    column(QuantityOrdComp; Quantity)
                    {

                    }
                    column(LocationCode; "Location Code")
                    {

                    }
                    column(UnitCost; "Unit Cost")
                    {

                    }
                    trigger OnAfterGetRecord()
                    var
                        rlItem: Record Item;
                    begin

                        // if rlItem.get("Item No.") then begin
                        //     repEtiquetaProducto.SetDatosF(rlItem, Round("Prod. Order Component"."Quantity per"));
                        //     repEtiquetaProducto.Run();
                        // end;
                    end;
                }
            }

            trigger OnAfterGetRecord()
            var

            begin
                ObtenerComentariosF();
            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    var
        xComentarios: Text;
        repEtiquetaProducto: Report TCN_EtiquetaProducto;

    local procedure ObtenerComentariosF()
    var
        rlProdOrderCommentLine: Record "Prod. Order Comment Line";
    begin
        rlProdOrderCommentLine.SetRange(Status, "Production Order".Status);
        rlProdOrderCommentLine.SetRange("Prod. Order No.", "Production Order"."No.");

        if rlProdOrderCommentLine.FindSet() then begin
            repeat
                xComentarios += rlProdOrderCommentLine.Comment + ';';
            until rlProdOrderCommentLine.Next() = 0;
        end;
        xComentarios := DelChr(xComentarios, '>', ';');
    end;
}