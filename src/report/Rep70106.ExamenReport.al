report 70106 "ExamenReport"
{
    Caption = 'Examen Report';
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'ExamenReport.rdl';
    PreviewMode = PrintLayout;


    dataset
    {
        dataitem(Job; Job)
        {
            RequestFilterFields = "No.";
            column(No_; "No.") { }
            column(Description; Description) { }
            column(Bill_to_Customer_No_; "Bill-to Customer No.") { }
            column(Status; Status) { }
            // column(Bill_to_Address; "Bill-to Address") { }
            column(xCusAddre1; xCusAddre[1]) { }
            column(xCusAddre2; xCusAddre[2]) { }
            column(xCusAddre3; xCusAddre[3]) { }
            column(xCusAddre4; xCusAddre[4]) { }
            column(xCusAddre5; xCusAddre[5]) { }
            column(xCusAddre6; xCusAddre[6]) { }
            column(xCusAddre7; xCusAddre[7]) { }
            column(xCusAddre8; xCusAddre[8]) { }
            column(reCustomer; reCustomer.Name) { } //para ver algo en el pie

            dataitem("Job Task"; "Job Task")
            {
                DataItemLink = "Job No." = field ("No.");
                column(Job_Task_No_; "Job Task No.") { }
                column(DescriptionTask; Description) { }
                column(Job_Task_Type; "Job Task Type") { }
                column(Schedule__Total_Cost_; "Schedule (Total Cost)") { } //campoPresupuesto 10

                dataitem("Job Planning Line"; "Job Planning Line")
                {
                    DataItemLink = "Job No." = field ("Job No."), "Job Task No." = field ("Job Task No.");
                    column(Line_No_; "Line No.") { }
                    column(Line_Type; "Line Type") { }
                    column(Type; Type) { }
                    column(DescriptionPlaning; Description) { }
                    column(QuantityPlaning; Quantity) { }

                    trigger OnAfterGetRecord()
                    var
                        rlJobPlanningLine: Record "Job Planning Line";
                    begin
                        rlJobPlanningLine.SetRange("Line No.", "Line No.");
                        rlJobPlanningLine.SetRange("Job No.", "Job No.");
                        rlJobPlanningLine.SetFilter("No.", '<>%1', '');
                        if rlJobPlanningLine.IsEmpty then
                            CurrReport.Skip();  //Skip salta
                    end;
                }

                dataitem("Job Ledger Entry"; "Job Ledger Entry")
                {
                    DataItemLink = "Job No." = field ("Job No."), "Job Task No." = field ("Job Task No.");
                    column(Ledger_Entry_No_; "Ledger Entry No.") { }
                    column(Ledger_Entry_Type; "Ledger Entry Type") { }
                }

                trigger OnAfterGetRecord()
                var
                begin
                    CalcFields("Schedule (Total Cost)");
                end;

            }


            trigger OnAfterGetRecord()
            begin
                reCustomer.Get(Job."Bill-to Customer No.");
                ObtenerDireccionesF();
            end;

        }
    }

    requestpage
    {
        SaveValues = true;
        layout
        {
            area(Content)
            {
                group(GroupName)
                {

                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    var
        myInt: Integer;
        xCusAddre: array[8] of Text;
        cuFormatAddress: Codeunit "Format Address";
        reCustomer: Record Customer;

    local procedure ObtenerDireccionesF()
    begin
        cuFormatAddress.Customer(xCusAddre, reCustomer);
    end;


}