report 70100 "TCN_ListadoHistPresencia"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'ListHistorico.rdl';
    dataset
    {
        dataitem(Employee; Employee)
        {
            RequestFilterFields = "No.";
            PrintOnlyIfDetail = true;

            column(Name; Name)
            {
            }
            column(No; "No.")
            {
            }

            column(TCN_HorasPresenciaAusencia; TCN_HorasPresenciaAusencia)
            {
            }
            column(TCN_TipoFichaje; TCN_TipoFichaje)
            {
            }
            column(TotalHoras; xTotalHoras)
            {

            }
            column(xFiltros; xFiltros)
            {

            }
            dataitem(TCN_HistoticoPresencia; TCN_HistoticoPresencia)
            {

                DataItemLink = TCN_NumeroEmpleado = field ("No.");
                DataItemTableView = sorting (TCN_NumeroOrden);
                column(TCN_CodAusencia; TCN_CodAusencia)
                {
                }
                column(TCN_CodTurno; TCN_CodTurno)
                {
                }
                column(TCN_DescAusencia; TCN_DescAusencia)
                {
                }
                column(TCN_FechaFin; TCN_FechaFin)
                {
                }
                column(TCN_FechaInicio; TCN_FechaInicio)
                {
                }
                column(TCN_Fin; TCN_Fin)
                {
                }
                column(TCN_HoraFin; Format(TCN_HoraFin))
                {
                }
                column(TCN_HoraInicio; Format(TCN_HoraInicio))
                {
                }
                column(TCN_Inicio; TCN_Inicio)
                {
                }
                column(TCN_NumeroEmpleado; TCN_NumeroEmpleado)
                {
                }
                column(TCN_NumeroHoras; TCN_NumeroHoras)
                {
                }
                column(TCN_NumeroOrden; TCN_NumeroOrden)
                {
                }
                column(TCN_TipoMovimiento; TCN_TipoMovimiento)
                {
                }

                trigger OnAfterGetRecord()
                begin
                    xTotalHoras += TCN_NumeroHoras;
                end;


            }
        }

    }
    requestpage
    {
        layout
        {
            area(content)
            {
                group(GroupName)
                {
                    field("Mostar horas black"; xMostrarHorasBlack)
                    {

                    }
                }
            }
        }
        actions
        {
            area(processing)
            {
            }
        }
    }

    var
        xMostrarLinea: Boolean;
        xMostrarHorasBlack: Boolean;
        xTotalHoras: Decimal;
        xFiltros: Text;


    trigger OnPreReport()
    begin
        xFiltros := 'Filtro: ' + Employee.GetFilters + '';
        xFiltros += TCN_HistoticoPresencia.GetFilters;
    end;
}
