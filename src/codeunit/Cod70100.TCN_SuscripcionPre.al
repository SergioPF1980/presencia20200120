codeunit 70100 "TCN_SuscripcionPre"
{
    SingleInstance = true;
    EventSubscriberInstance = StaticAutomatic;

    [EventSubscriber(ObjectType::Page, Page::"Sales Quote", 'OnBeforeActionEvent', 'Statistics', false, false)]
    local procedure OnBeforeActionEventStatisticsPageSalesQuoteF()
    begin
        Message('Mi Código');
        Commit();
        Error('');  //No muestra ningun mensaje de error
    end;

    // [EventSubscriber(ObjectType::Table, Database::"Cause of Absence", 'OnBeforeDeleteEvent', '', false, false)]
    // local procedure OnBeforeDeleteEventTableCauseOfAbsenceF(var Rec: Record "Cause of Absence")
    // var
    //     rlTCN_HistoticoPresencia: Record TCN_HistoticoPresencia;
    //     clError: Label 'Existe una Ausencvia Registrada con esta ausencia %1, imposible borrar';
    // begin
    //     rlTCN_HistoticoPresencia.SetRange(TCN_CodAusencia, Rec.code);
    //     // FindFirst
    //     // if rlTCN_HistoticoPresencia.FindFirst() then begin
    //     //     Error(clError, rec.code);
    //     // end;

    //     //si hay muchos registros es preferible IsEmpty, para evitar trafico.solo devuelve un booleano
    //     if not rlTCN_HistoticoPresencia.IsEmpty then begin
    //         Error(clError, rec.code);
    //     end;
    // end;
}
