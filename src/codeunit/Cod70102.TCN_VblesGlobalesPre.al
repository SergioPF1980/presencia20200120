codeunit 70102 "TCN_VblesGlobalesPre"
{

    SingleInstance = true;

    EventSubscriberInstance = StaticAutomatic;//es necesaria porque añadimos las variables, si no desaparecerian.


    var
        xEmpleado: Code[20];
        xInicio: DateTime;
        xCodAusencia: Text;

        xLimpiar: Boolean;

    procedure SetPasaPArametrosF(pEmpleado: code[20]; pFechaInicio: DateTime; pLimpiar: Boolean)
    begin
        xEmpleado := pEmpleado;
        xInicio := pFechaInicio;
        xLimpiar := pLimpiar;
    end;

    procedure GetPasaPArametrosF(var pEmpleado: code[20]; var pFechaInicio: DateTime; var pLimpiar: Boolean)
    begin
        pEmpleado := xEmpleado;
        pFechaInicio := xInicio;
        pLimpiar := xLimpiar;
    end;

    // Sobrecarga, misma funcion con un parametro mas
    procedure SetPasaPArametrosF(pEmpleado: code[20]; pFechaInicio: DateTime; pCodAusencia: Code[10]; pLimpiar: Boolean)
    begin
        // xEmpleado := pEmpleado;
        // xInicio := pFechaInicio;
        SetPasaPArametrosF(pEmpleado, pFechaInicio, pLimpiar);
        xCodAusencia := pCodAusencia;
    end;

    procedure GetPasaPArametrosF(var pEmpleado: code[20]; var pFechaInicio: DateTime; var pCodAusencia: Code[10]; var pLimpiar: Boolean)
    begin
        // pEmpleado := xEmpleado;
        // pFechaInicio := xInicio;
        GetPasaPArametrosF(pEmpleado, pFechaInicio, pLimpiar);
        pCodAusencia := xCodAusencia;
    end;



}