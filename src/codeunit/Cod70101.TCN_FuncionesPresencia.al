codeunit 70101 "TCN_FuncionesPresencia"
{   //Logica de Negocio
    // Permissions = tabledata "Sales Invoice Header" = md,
    //               tabledata "Sales Invoice Line" = md;

    //Funcion para saber si es Super Usuario
    procedure EsSuperUPreF() xSalida: Boolean;
    var
        rlTCN_UsuariosPre: Record TCN_UsuariosPre;
    begin
        if rlTCN_UsuariosPre.Count = 0 then begin
            xSalida := true;
        end else begin
            if rlTCN_UsuariosPre.Get(UserId) then begin
                xSalida := rlTCN_UsuariosPre.SuperPresencia;
            end else begin
                xSalida := false;
            end;
        end;
    end;

    procedure nombreEmpleadoF(pCodigo: Code[20]) xSalida: Text
    var
        rlEmployee: Record Employee;
    begin
        if rlEmployee.Get(pCodigo) then begin
            xSalida := rlEmployee.FullName();
        end;
    end;

    procedure descripcionAusenciaF(pCodigo: Code[10]) xSalida: Text
    var
        rlCauseOfAbsence: Record "Cause of Absence";
    begin
        if rlCauseOfAbsence.Get(pCodigo) then begin
            xSalida := rlCauseOfAbsence.Description;
        end;
    end;

    procedure turnoEmpleadoF(pTurno: Code[10]) xSalida: Code[10]
    var
        rlTempEmployee: Record Employee;
    begin
        /* sabemos el turno del empleado */
        if rlTempEmployee.Get(pTurno) then begin
            xSalida := rlTempEmployee.TCN_CodTurno;
        end;
    end;

    procedure diasTurnoF() xSalida: Integer;
    var
        rlTablaConfPresencia: Record TCN_TablaConfPresencia;
        //xDias: Boolean;

    begin
        rlTablaConfPresencia.getF();
        xSalida := 0;
        if rlTablaConfPresencia.Lunes then begin
            xSalida += 1;
        end;
        if rlTablaConfPresencia.Martes then begin
            xSalida += 1;
        end;
        if rlTablaConfPresencia.Miercoles then begin
            xSalida += 1;
        end;
        if rlTablaConfPresencia.Jueves then begin
            xSalida += 1;
        end;
        if rlTablaConfPresencia.Viernes then begin
            xSalida += 1;
        end;
        if rlTablaConfPresencia.Sabado then begin
            xSalida += 1;
        end;
        if rlTablaConfPresencia.Domingo then begin
            xSalida += 1;
        end;

    end;

    procedure HorasTurnoF(pTurno: Code[10]) xSalida: Decimal
    var

        rlHorarioTurno: Record TCN_HorarioTurno;
        rlTablaTurno: Record TCN_TablaTurno;
        xHorarioTurno: Decimal;
        xDescanso: Integer;

    begin
        if rlTablaTurno.Get(pTurno) then begin
            xDescanso := rlTablaTurno.MinutosDescanodesayuno;

        end;
        if rlHorarioTurno.Get(pTurno) then begin
            xHorarioTurno := (rlHorarioTurno.TCN_HoraFinal - rlHorarioTurno.TCN_HoraInicio) / 3600000;
            xSalida := xHorarioTurno - (xDescanso / 60);
        end;
        /*-----------------------------------------------------------------------------------------*/
        //  Jose, 12 de la noche
        // if rlHorarioTurno.TCN_HoraFinal < rlHorarioTurno.TCN_HoraInicio then begin
        //     xSalida := 0T - rlHorarioTurno.TCN_HoraInicio;
        //     xSalida += rlHorarioTurno.TCN_HoraFinal - 0T;
        // end else begin
        //     xSalida := rlHorarioTurno.TCN_HoraFinal - rlHorarioTurno.TCN_HoraInicio;
        // end;
        //  Jose, sin 12 de la noche
        // xSalida:= rlHorarioTurno.TCN_HoraFinal - rlHorarioTurno.TCN_HoraInicio;
        // if rlHorarioTurno.Get(pTurno) then begin
        //     xSalida -= (rlTablaTurno.MinutosDescanodesayuno * 360000);
        // end;
    end;


    //ini
    procedure IniFinSemanaF(pDate: Date; var pDiaInicio: Date; var pDiaFin: Date)
    var
        xlSemana: Integer;
        xlAnio: Integer;
    begin
        xlSemana := Date2DWY(pDate, 2);
        xlAnio := Date2DWY(pDate, 3);
        pDiaInicio := DWY2Date(1, xlSemana, xlAnio);
        pDiaFin := DWY2Date(7, xlSemana, xlAnio);
    end;


}