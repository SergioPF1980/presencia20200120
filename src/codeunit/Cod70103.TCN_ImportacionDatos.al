codeunit 70103 "TCN_ImportacionDatos"
{
    Permissions = tabledata "TCN_HistoticoPresencia" = rimd;

    trigger OnRun()
    var
        rlTempBlobTMP: Record TempBlob temporary;
        xlInStream: InStream;
        xlFichero: Text;
        xllinea: Text;
        xlNumCampo: Integer;
        rlTCN_HistoticoPresencia: Record TCN_HistoticoPresencia;
        xlCampo: Text;
        xlContLinea: Integer;
        rlTempCommentLine: Record "Comment Line" temporary; // tabla temporal para las incidencias



    begin
        rlTempBlobTMP.Blob.CreateInStream(xlInStream, TextEncoding::Windows);

        if UploadIntoStream('Seleccione el fichero', '',
                             'Archivos de permitidos(*.txt,*.csv)|*.txt;*.csv| Todos los archivos (*.*)|*.*',
                             xlFichero, xlInStream) then begin


            // leemos la primera linea en blanco porque no queremos añadirla
            while not xlInStream.EOS do begin   //EOs es el final del fichero
                xlInStream.ReadText(xllinea);
                xlContLinea += 1;   //Contador + 1

                if xlContLinea <> 1 then begin

                    //xlNumCampo := 0; // el campo es cero, no ha leido ninguno
                    rlTCN_HistoticoPresencia.Init();
                    /*
                    foreach 
                    llamamos a la funcion AsignarDatos donde esta el foreach
                    */
                    if AsignarDatosF(rlTCN_HistoticoPresencia, xllinea) then begin
                        rlTCN_HistoticoPresencia.Insert(true);

                    end /* IF */ else begin
                        rlTempCommentLine.Init();
                        rlTempCommentLine."Line No." := xlContLinea;    //le meto la linea porque Linea es clave
                        // rlTempCommentLine.Comment := CopyStr(GetLastErrorText, 1, MaxStrLen(rlTempCommentLine.Comment));    //GetLastErrorText me devuelve el error. para controlar el stream. por defecto es 80
                        rlTempCommentLine.Comment := CopyStr(StrSubstNo('Lin %1, errror %2', rlTCN_HistoticoPresencia.TCN_NumeroOrden,
                                                            GetLastErrorText), 1, MaxStrLen(rlTempCommentLine.Comment));

                        rlTempCommentLine.Insert(false);    //Como me da un error no inserto los datos
                    end;    // else
                end;    // if
            end;  // while
            if rlTempCommentLine."Line No." = 0 then begin
                Message('Proceso finalizado');
            end else begin

            end;
        end;    // if
        Page.Run(0, rlTempCommentLine);
    end;

    local procedure PasaTextoADateTimeF(pTxtoFecha: Text) xSalida: DateTime
    var
        xlDia: Integer;
        xlMes: Integer;
        xlAño: Integer;
        xlTime: Time;

    begin
        if pTxtoFecha <> '' then begin
            // 01 02 2020 1212 -------1212 es la hora, 12 horas 12 minutos
            Evaluate(xlDia, CopyStr(pTxtoFecha, 1, 2)); //Posicion y longitud
            Evaluate(xlMes, CopyStr(pTxtoFecha, 3, 2));
            Evaluate(xlAño, CopyStr(pTxtoFecha, 5, 4));
            Evaluate(xlTime, CopyStr(pTxtoFecha, 9, 4));

            xSalida := CreateDateTime(DMY2Date(xlDia, xlMes, xlAño), xlTime);
        end else begin
            xSalida := 0DT;
        end;

    end;

    [TryFunction]
    local procedure AsignarDatosF(var pHisPresencia: Record TCN_HistoticoPresencia; plLinea: Text)
    var
        xlNumCampo: Integer;
        xlCampo: Text;
    begin
        xlNumCampo := 0;
        foreach xlCampo in plLinea.Split(';') do begin
            xlNumCampo += 1;

            case xlNumCampo of
                1:
                    begin
                        Evaluate(pHisPresencia.TCN_NumeroOrden, xlCampo);
                        pHisPresencia.Validate(TCN_NumeroOrden);
                    end;
                2:
                    begin
                        Evaluate(pHisPresencia.TCN_NumeroEmpleado, xlCampo);
                        pHisPresencia.Validate(TCN_NumeroEmpleado, xlCampo);
                    end;
                3:
                    begin
                        xlCampo := UpperCase(xlCampo);
                        case xlCampo of
                            'A':
                                begin
                                    pHisPresencia.Validate(TCN_TipoMovimiento, pHisPresencia.TCN_TipoMovimiento::Ausencia);

                                end;
                            'P':
                                begin
                                    pHisPresencia.Validate(TCN_TipoMovimiento, pHisPresencia.TCN_TipoMovimiento::Presencia);
                                end;
                            else
                                Error('Error no hay tipo de movimiento');
                        end;
                    end;
                4:
                    begin
                        //Evaluate(rlTCN_HistoticoPresencia.TCN_CodAusencia, xlCampo);
                        pHisPresencia.Validate(TCN_CodAusencia, xlCampo);

                    end;
                5:
                    begin
                        pHisPresencia.Validate(TCN_Inicio, PasaTextoADateTimeF(xlCampo));

                    end;
                6:
                    begin

                        pHisPresencia.Validate(TCN_Fin, PasaTextoADateTimeF(xlCampo));

                    end;
            end; // end del case
        end;    // del foreach
    end;    //end del begin principal


}