enum 70100 "TCN_TipoFichaje"
{
    Extensible = true;
    
    value(0; " ")
    {
        Caption = ' ';
    }
    value(10; Presencia)
    {
        Caption = 'Presencia';
    }
    value(20; Ausencia)
    {
        Caption = 'Ausencia';
    }
    
}
