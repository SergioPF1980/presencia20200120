tableextension 70102 "TCN_CausesOfAbsence" extends "Cause of Absence" //MyTargetPageId
{
    fields
    {
        field(10; TCN_Pre_Null; Boolean)
        {

        }

    }
    trigger OnDelete()
    var
        rlTCN_HistoticoPresencia: Record TCN_HistoticoPresencia;
        clError: Label 'Existe una Ausencvia Registrada con esta ausencia %1, imposible borrar';
    begin
        rlTCN_HistoticoPresencia.SetRange(TCN_CodAusencia, Rec.code);

        if not rlTCN_HistoticoPresencia.IsEmpty() then begin
            Error(clError, rec.code);
        end;
    end;


    // var
    //     rlTCN_HistoticoPresencia: Record TCN_HistoticoPresencia;
    //     clError: Label 'Existe una Ausencvia Registrada con esta ausencia %1, imposible borrar';
    // begin
    //     rlTCN_HistoticoPresencia.SetRange(TCN_CodAusencia, Rec.code);
    //--------------------------------------------------------------
    //     // FindFirst
    //     // if rlTCN_HistoticoPresencia.FindFirst() then begin
    //     //     Error(clError, rec.code);
    //     // end;

    //     //si hay muchos registros es preferible IsEmpty, para evitar trafico.solo devuelve un booleano
    //     if not rlTCN_HistoticoPresencia.IsEmpty then begin
    //         Error(clError, rec.code);
    //     end;
    // end;
}