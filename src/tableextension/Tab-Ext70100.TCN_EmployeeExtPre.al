tableextension 70100 "TCN_EmployeeExtPre" extends Employee //MyTargetTableId
{
    fields
    {
        field(70100; TCN_CodTurno; Code[10])
        {
            Caption = 'Código Turno';
            TableRelation = "TCN_TablaTurno".Codigo;
        }

        field(70101; TCN_TipoFichaje; Enum TCN_TipoFichaje)
        {
            Caption = 'Tipo Fichaje';
            FieldClass = FlowFilter;
        }

        field(70102; TCN_HorasPresenciaAusencia; Decimal)
        {
            Caption = 'Nº Horas Presencia/Ausencia';
            FieldClass = FlowField;
            CalcFormula = sum (TCN_HistoticoPresencia.TCN_NumeroHoras where (TCN_NumeroEmpleado = field ("No."),
                                                                            TCN_FechaInicio = field ("Date Filter"),
                                                                            TCN_TipoMovimiento = field (TCN_TipoFichaje)

                                                                        ));
            Editable = false;
        }
    }

    trigger OnDelete()

    begin
        TestField(TCN_CodTurno, '');
    end;



}