pageextension 70102 "TCN_SalesInvoiceListExt" extends "Posted Sales Invoices" //MyTargetPageId
{
    layout
    {

    }

    actions
    {
        addafter(Print)
        {
            action(ImprimirFacturaPersonalizada)
            {
                Caption = 'Imprimir Factura Personalizada';
                trigger OnAction()
                var
                    rlSalesInvoiceHeader: Record "Sales Invoice Header";
                    rlFacturaVenta: Report TCN_FacturaVenta;
                begin
                    CurrPage.SetSelectionFilter(rlSalesInvoiceHeader);
                    rlFacturaVenta.SetTableView(rlSalesInvoiceHeader);
                    rlFacturaVenta.RunModal();
                end;
            }
        }
    }
}