page 70107 "TCN_TurnoDrillDown"
{
    Caption = 'Turno DrillDown';
    PageType = List;
    Editable = false;
    SourceTable = TCN_TablaTurno;

    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;
                }
            }
        }

    }
}