page 70104 "TCN_FactBoxHorarioTurnos"
{
    Caption = 'HorarioTurnos';
    PageType = ListPart;

    SourceTable = TCN_HorarioTurno;

    layout
    {
        area(Content)
        {
            repeater(Group)
            {


                field(TCN_CodigoTurno; TCN_CodigoTurno)
                {
                    ApplicationArea = All;
                }
                field(TCN_HoraInicio; TCN_HoraInicio)
                {
                    ApplicationArea = All;
                }
                field(TCN_HoraFinal; TCN_HoraFinal)
                {
                    ApplicationArea = All;
                }
            }
        }

    }

}