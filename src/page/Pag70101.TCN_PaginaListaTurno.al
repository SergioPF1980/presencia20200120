page 70101 "TCN_PaginaListaTurno"
{

    PageType = List;
    SourceTable = "TCN_TablaTurno";    // objeto Rec y xRec
    Caption = 'Lista Turno';
    ApplicationArea = All;
    UsageCategory = Lists;
    CardPageId = TCNPaginaCardTurno;
    Editable = false;   //para que no se edite


    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;
                }
                field(Descripcion; Descripcion)
                {
                    ApplicationArea = All;
                }
                field(MinutosDescanodesayuno; MinutosDescanodesayuno)
                {
                    ApplicationArea = All;
                }
                field(FechaAlta; FechaAlta)
                {
                    ApplicationArea = All;
                    Visible = false;
                }
                field(FechaModificacion; FechaModificacion)
                {
                    ApplicationArea = All;
                    Visible = false;
                }
                field(UsuarioAlta; UsuarioAlta)
                {
                    ApplicationArea = All;
                    Visible = false;
                }
                field(UsuarioModificacion; UsuarioModificacion)
                {
                    ApplicationArea = All;
                    Visible = false;
                }
                field(NumEmpleadoAsig; NumEmpleadoAsig)
                {
                    ApplicationArea = All;
                    Visible = false;
                }
            }
        }
        area(FactBoxes)
        {
            part(InfTurno; TCN_FactBox)
            {
                SubPageLink = Codigo = field (Codigo);
            }

            part(Horarios; TCN_FactBoxHorarioTurnos)
            {
                SubPageLink = TCN_CodigoTurno = field (Codigo);
            }
        }
    }

}
