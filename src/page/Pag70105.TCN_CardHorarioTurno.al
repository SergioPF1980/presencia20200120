page 70105 "TCN_CardHorarioTurno"
{
    Caption = 'Horario Turno';
    PageType = Card;
    SourceTable = TCN_HorarioTurno;
    //Editable = true;    //true por defecto

    layout
    {
        area(Content)
        {
            group(GroupName)
            {

                field(TCN_CodigoTurno; TCN_CodigoTurno)
                {
                    ApplicationArea = All;
                }
                field(TCN_HoraFinal; TCN_HoraFinal)
                {
                    ApplicationArea = All;
                }
                field(TCN_HoraInicio; TCN_HoraInicio)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin

                end;
            }
        }
    }

    var
        myInt: Integer;
}