page 70102 "TCNPaginaCardTurno"
{

    PageType = Card;
    SourceTable = "TCN_TablaTurno";
    Caption = 'Pagina Card Turno';

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;
                }
                field(Descripcion; Descripcion)
                {
                    ApplicationArea = All;
                }
                field(MinutosDescanodesayuno; MinutosDescanodesayuno)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}
