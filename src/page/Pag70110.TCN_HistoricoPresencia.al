page 70110 "TCN_HistoricoPresencia"
{

    PageType = List;
    SourceTable = TCN_HistoticoPresencia;
    Caption = 'Historico Presencia';
    ApplicationArea = All;
    UsageCategory = History;
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(TCN_NumeroEmpleado; TCN_NumeroEmpleado)
                {
                    ApplicationArea = All;
                }
                field(TCN_CodTurno; TCN_CodTurno)
                {
                    ApplicationArea = All;
                }
                field(TCN_CodAusencia; TCN_CodAusencia)
                {
                    ApplicationArea = All;
                }

                field(TCN_DescAusencia; TCN_DescAusencia)
                {
                    ApplicationArea = All;
                }

                field(TCN_Inicio; TCN_Inicio)
                {
                    ApplicationArea = All;
                    trigger OnValidate()
                    begin
                        CurrPage.Update(true);
                    end;
                }
                field(TCN_Fin; TCN_Fin)
                {
                    ApplicationArea = All;
                    trigger OnValidate()
                    begin
                        CurrPage.Update(true);
                    end;
                }
                field(TCN_FechaInicio; TCN_FechaInicio)
                {
                    ApplicationArea = All;
                }
                field(TCN_FechaFin; TCN_FechaFin)
                {
                    ApplicationArea = All;
                }
                field(TCN_HoraInicio; TCN_HoraInicio)
                {
                    ApplicationArea = All;
                }
                field(TCN_HoraFin; TCN_HoraFin)
                {
                    ApplicationArea = All;
                }
                field(TCN_NumeroHoras; TCN_NumeroHoras)
                {
                    ApplicationArea = All;
                }
                field(TCN_NumeroOrden; TCN_NumeroOrden)
                {
                    ApplicationArea = All;
                    Visible = false;
                }
                field(TCN_TipoMovimiento; TCN_TipoMovimiento)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action("Importar CSV")
            {
                trigger OnAction()
                var
                    clImportacionDatos: Codeunit TCN_ImportacionDatos;
                begin
                    clImportacionDatos.Run();
                end;
            }
            action("Imprimir Listado Presencia")
            {
                trigger OnAction()
                var
                    ListadoHistPresencia: Report TCN_ListadoHistPresencia;
                begin
                    ListadoHistPresencia.Run();
                end;
            }
        }
    }

    // trigger OnOpenPage()
    // var
    //     clFuncionesPresencia: Codeunit TCN_FuncionesPresencia;

    // begin

    //     // CurrPage.Editable(clFuncionesPresencia.EsSuperUPreF());

    //     // Si eres super usuario 
    //     if clFuncionesPresencia.EsSuperUPreF() then begin
    //         xEditable := true;

    //     end else begin
    //         //todo
    //         xEditable := false;
    //         FilterGroup := FilterGroup + 2;
    //         Setrange(TCN_NumeroEmpleado, clFuncionesPresencia.BuscarEmpleadoF());
    //         FilterGroup := FilterGroup - 2;
    //     end;
    // end;

    trigger OnDeleteRecord(): Boolean
    begin
        exit(xEditable);
    end;


    // Funcion Buscar empleado correspondiente a 2 tablas y devolvemos el numero de empleado
    procedure BuscarEmpleadoF() xSalida: code[20]
    var
        // Necesitamos los registros de las tablas
        rlResource: Record Resource;
        rlEmployee: Record Employee;

    begin
        xSalida := 'Error, no existe el registro';
        // Comprobamos que hay registro
        rlResource.SetRange("Time Sheet Owner User ID", UserId);
        // buscamos en el primer registro
        if rlResource.FindFirst() then begin
            //Debemos buscar en la otra tabla
            rlEmployee.SetRange("Resource No.", rlResource."No.");
            if rlEmployee.FindFirst() then begin
                xSalida := rlEmployee."No.";
            end;
        end;
    end;



    var
        xEditable: Boolean;
}
