page 70109 "TCN_CuesPresenscia"
{
    Caption = 'Cues Presenscia';
    PageType = CardPart;

    layout
    {
        area(Content)
        {
            cuegroup(GroupName)
            {

                field(FichajeEntrada; EntradaF())
                {
                    Caption = 'Entrada';
                    Image = "Key";

                    trigger OnDrillDown()
                    var

                    begin
                        // OnPushEntradaF();
                        // cugVblesGlobalesPre.GetPasaPArametrosF(xEmpleado, xInicio);
                        // if xEmpleado = '' then begin
                        //     Error('Especifica el empleado');
                        // end;
                        // if xInicio = 0DT then begin
                        //     Error('Especifica el Hora');
                        // end;
                        // rHistoticoPresencia.Init();
                        // rHistoticoPresencia.Validate(TCN_TipoMovimiento, rHistoticoPresencia.TCN_TipoMovimiento::Presencia);
                        // rHistoticoPresencia.Validate(TCN_NumeroEmpleado, xEmpleado);
                        // rHistoticoPresencia.Validate(TCN_Inicio, xInicio);
                        // rHistoticoPresencia.Insert(true);
                        // cugVblesGlobalesPre.SetPasaPArametrosF('', 0DT);
                        // Message('Ha realizado el Fichaje');

                        EntradaF();
                        Clear(xCodAusencia);    //limpiar la variable
                        InsercionREgistrosHcoF(rHistoticoPresencia.TCN_TipoMovimiento::Presencia);
                    end;

                }
                field(FichajeSalida; SalidaF())
                {
                    Caption = 'Salida';
                    Image = Diagnostic;

                    trigger OnDrillDown()
                    var
                    begin
                        FicharFinF(rHistoticoPresencia.TCN_TipoMovimiento::Presencia);
                    end;
                }
                field(FichajeAusencia; InicioAusenciaF())
                {
                    Caption = 'Inicio Ausencia';
                    Image = Funnel;

                    trigger OnDrillDown()
                    begin
                        InsercionREgistrosHcoF(rHistoticoPresencia.TCN_TipoMovimiento::Ausencia);

                    end;

                }
                field(FichajeFinAusencia; FinAusenciaF())
                {
                    Caption = 'Fin Ausencia';
                    Image = Diagnostic;

                }
            }
            cuegroup(Informacion)
            {
                field(NumHorasSemanas; NumHorasSemanasF())
                {
                    Caption = 'Nº Horas Semana';
                    Image = Info;

                }
                field(NumHorasFichadasSemanas; NumHorasFichadasSemanasF())
                {
                    Caption = 'Nº Horas Fichadas Semana';
                    Image = Calculator;
                }

                actions
                {
                    action(ListaTurnos)
                    {
                        Caption = 'Lista Turnos';
                        RunObject = page TCN_PaginaListaTurno;
                    }
                }

            }
        }
    }

    var
        rHistoticoPresencia: Record TCN_HistoticoPresencia;
        cugVblesGlobalesPre: Codeunit TCN_VblesGlobalesPre;
        xEmpleado: Code[20];
        xInicio: DateTime;
        xCodAusencia: Code[10];
        xLimpiar: Boolean;



    local procedure EntradaF() xSalida: Integer //para que aparecca el numero en el cuadrado
    var

    begin

        xSalida := CalcularDatosF(rHistoticoPresencia.TCN_TipoMovimiento::Presencia, false);

    end;

    local procedure SalidaF() xSalida: Integer //para que aparecca el numero en el cuadrado
    var
    begin
        xSalida := CalcularDatosF(rHistoticoPresencia.TCN_TipoMovimiento::Presencia, true);
    end;

    local procedure InicioAusenciaF() xSalida: Integer //para que aparecca el numero en el cuadrado
    begin

        xSalida := CalcularDatosF(rHistoticoPresencia.TCN_TipoMovimiento::Ausencia, false);
    end;

    local procedure FinAusenciaF() xSalida: Integer //para que aparecca el numero en el cuadrado
    begin
        xSalida := CalcularDatosF(rHistoticoPresencia.TCN_TipoMovimiento::Ausencia, true);

    end;

    local procedure NumHorasSemanasF() xSalida: Decimal //para que aparecca el numero en el cuadrado
    var
        culFuncionesPresencia: Codeunit TCN_FuncionesPresencia;
    begin
        cugVblesGlobalesPre.GetPasaPArametrosF(xEmpleado, xInicio, xCodAusencia, xLimpiar);
        xSalida := culFuncionesPresencia.diasTurnoF() * culFuncionesPresencia.HorasTurnoF(culFuncionesPresencia.turnoEmpleadoF(xEmpleado));
    end;

    local procedure NumHorasFichadasSemanasF() xSalida: Decimal //para que aparecca el numero en el cuadrado
    var
        culFuncionesPresencia: Codeunit TCN_FuncionesPresencia;
        culVblesGlobalesPre: Codeunit TCN_VblesGlobalesPre;
        rlHistoticoPresencia: Record TCN_HistoticoPresencia;

        xDiaInicio: Date;
        xDiaFin: Date;

    begin
        culVblesGlobalesPre.GetPasaPArametrosF(xEmpleado, xInicio, xLimpiar); // para saber los datos

        if xInicio <> 0DT then begin
            culFuncionesPresencia.IniFinSemanaF(DT2Date(xInicio), xDiaInicio, xDiaFin);
            rlHistoticoPresencia.SetRange(TCN_FechaInicio, xDiaInicio, xDiaFin);
            rlHistoticoPresencia.SetRange(TCN_NumeroEmpleado, xEmpleado);
            rlHistoticoPresencia.SetRange(TCN_TipoMovimiento, rlHistoticoPresencia.TCN_TipoMovimiento::Presencia);
            rlHistoticoPresencia.SetRange(TCN_FechaInicio, xDiaInicio, xDiaFin);
            rlHistoticoPresencia.CalcSums(TCN_NumeroHoras);
            xSalida := rlHistoticoPresencia.TCN_NumeroHoras;
        end;


    end;

    local procedure InsercionREgistrosHcoF(pTipoMovimiento: Integer)
    begin
        cugVblesGlobalesPre.GetPasaPArametrosF(xEmpleado, xInicio, xCodAusencia, xLimpiar);

        if xEmpleado = '' then begin
            Error('Especifica el empleado');
        end;
        if xInicio = 0DT then begin
            Error('Especifica el Hora');
        end;

        if pTipoMovimiento = rHistoticoPresencia.TCN_TipoMovimiento::Ausencia then begin
            if xCodAusencia = '' then begin
                Error('Especifica la Ausencia');
            end;
        end;
        rHistoticoPresencia.Init();
        rHistoticoPresencia.Validate(TCN_NumeroOrden, 0);
        rHistoticoPresencia.Validate(TCN_TipoMovimiento, pTipoMovimiento);
        rHistoticoPresencia.Validate(TCN_NumeroEmpleado, xEmpleado);
        rHistoticoPresencia.Validate(TCN_Inicio, xInicio);  //coge el campo de la tabla His. Pre dnd calcula la fecha
        rHistoticoPresencia.Validate(TCN_CodAusencia, xCodAusencia);

        rHistoticoPresencia.Insert(true);
        cugVblesGlobalesPre.SetPasaPArametrosF('', 0DT, '', true);
        Message('Ha realizado el Fichaje');
        CurrPage.Update(false);
        EntradaF();
    end;

    local procedure CalcularDatosF(pTipoMovimiento: Integer; pSalida: Boolean) xSalida: Integer
    begin
        cugVblesGlobalesPre.GetPasaPArametrosF(xEmpleado, xInicio, xCodAusencia, xLimpiar);

        rHistoticoPresencia.SetRange(TCN_NumeroEmpleado, xEmpleado);
        rHistoticoPresencia.SetRange(TCN_TipoMovimiento, pTipoMovimiento);

        if pSalida then begin
            rHistoticoPresencia.SetRange(TCN_FechaFin, DT2Date(xInicio));
        end else begin
            rHistoticoPresencia.SetRange(TCN_FechaInicio, DT2Date(xInicio));
        end;
        xSalida := rHistoticoPresencia.Count;
    end;


    procedure FicharFinF(pTipoFichaje: Enum TCN_TipoFichaje)
    var
        culVblesGlobalesPre: Codeunit TCN_VblesGlobalesPre;
        rlHistoticoPresencia: Record TCN_HistoticoPresencia;

        // xInicioFichar: Date;
        // xHoraInicio: time;

    begin


        culVblesGlobalesPre.GetPasaPArametrosF(xEmpleado, xInicio, xLimpiar); // para saber los datos del em`pleadoç
        rlHistoticoPresencia.SetCurrentKey(TCN_TipoMovimiento, TCN_NumeroEmpleado, TCN_HoraFin, TCN_HoraInicio);// Ordenamos por la clave

        rHistoticoPresencia.SetRange(TCN_TipoMovimiento, pTipoFichaje);
        rHistoticoPresencia.SetRange(TCN_NumeroEmpleado, xEmpleado);
        rHistoticoPresencia.SetRange(TCN_Fin, 0DT);
        rlHistoticoPresencia.SetRange(TCN_Inicio, CreateDateTime(DT2Date(xInicio), 0T), xInicio);


        if not rlHistoticoPresencia.FindLast() then begin
            rlHistoticoPresencia.Init();
            rlHistoticoPresencia.Validate(TCN_TipoMovimiento, pTipoFichaje);
            rlHistoticoPresencia.Validate(TCN_NumeroEmpleado, xEmpleado);
            rlHistoticoPresencia.Insert(true);
        end;
        rlHistoticoPresencia.Validate(TCN_Fin, xInicio);
        rlHistoticoPresencia.Modify(true);

        culVblesGlobalesPre.SetPasaPArametrosF('', 0DT, '', true);

    end;

    // local procedure verificarDatosF()
    // var
    //     culVblesGlobalesPre: Codeunit TCN_VblesGlobalesPre;
    //     rlHistoticoPresencia: Record TCN_HistoticoPresencia;
    // begin
    //     cugVblesGlobalesPre.GetPasaPArametrosF(xEmpleado, xInicio, xCodAusencia, xLimpiar);
    //     if xEmpleado = '' then begin
    //         Error('Especifica el empleado');
    //     end;
    //     if xInicio = 0DT then begin
    //         Error('Especifica el Hora');
    //     end;

    //     if pTipoMovimiento = rHistoticoPresencia.TCN_TipoMovimiento::Ausencia then begin
    //         if xCodAusencia = '' then begin
    //             Error('Especifica la Ausencia');
    //         end;
    //     end;
    // end;








    /*------------------------------------------------------------------------------------------------*/
    // procedure PasaParametrosF(pEmpleado: Code[20]; pFechaInicio: DateTime)
    // begin
    //     xEmpleado := pEmpleado;
    //     xInicio := pFechaInicio;
    // end;

    [IntegrationEvent(false, false)]
    local procedure OnPushEntradaF()
    begin

    end;

}