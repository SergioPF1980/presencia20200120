page 70106 "TCN_ListaHorarioTurno"
{
    Caption = 'Lista Horario Turno';
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = TCN_HorarioTurno;
    Editable = false;
    CardPageId = TCN_CardHorarioTurno;

    layout
    {
        area(Content)
        {
            repeater(Group)
            {

                field(TCN_CodigoTurno; TCN_CodigoTurno)
                {
                    ApplicationArea = All;
                }
                field(TCN_HoraInicio; TCN_HoraInicio)
                {
                    ApplicationArea = All;
                }
                field(TCN_HoraFinal; TCN_HoraFinal)
                {
                    ApplicationArea = All;
                }
                field(NumTurnos; NumTurnos)
                {
                    ApplicationArea = All;
                }

                // field(NumHoras; )
                // {
                //     Caption = 'Nº Horas';
                //     ApplicationArea = All;
                // }
            }
        }
    }

}