page 70108 "TCN_Fichaje"
{
    PageType = Card;
    Caption = 'Fichaje';
    UsageCategory = Administration;
    ApplicationArea = all;

    layout
    {
        area(Content)
        {
            group(Datos)
            {
                field(Empleado; xEmpleado)
                {
                    Caption = 'Empleado';
                    TableRelation = Employee."No.";

                    trigger OnValidate()
                    var

                    begin

                        xFechaHoraInicio := RoundDateTime(CurrentDateTime, 1000, '=');
                        PasarDatosGlobalF();
                    end;
                }
                field(NombreEmpleado; cugFuncionesPresencia.nombreEmpleadoF(xEmpleado))
                {
                    Caption = 'Nombre Empleado';
                    TableRelation = Employee."No."; //Relacionamos la tabla y el campo de Empleados
                }
                field(FechaHora; xFechaHoraInicio)
                {
                    Caption = 'Fecha Hora Inicio';

                    trigger OnValidate()
                    var

                    begin
                        PasarDatosGlobalF();

                    end;

                }
                field(CodAusencia; xCodAusencia)
                {
                    Caption = 'Cod Ausencia';
                    TableRelation = "Cause of Absence".Code;
                    LookupPageId = "Causes of Absence";
                    DrillDownPageId = "Causes of Absence";

                    trigger OnValidate()
                    var
                        rlCauseofAbsence: Record "Cause of Absence";
                    begin
                        if rlCauseofAbsence.Get(xCodAusencia) then begin
                            xDescAusencia := rlCauseofAbsence.Description;
                        end else begin
                            xDescAusencia := '';
                        end;

                        PasarDatosGlobalF();
                    end;
                }
                field(DescAusencia; cugFuncionesPresencia.descripcionAusenciaF(xCodAusencia))
                {
                    Caption = 'Descripcion';


                }

                field(FechaHoraFin; xFechaHoraFin)
                {
                    Caption = 'Fecha Hora Fin';
                }


            }
            group(cues)
            {
                part(Cues1; TCN_CuesPresenscia)
                {
                    UpdatePropagation = Both;
                }
            }
        }
    }
    var
        xEmpleado: Code[20];
        xNombreEmpleado: Text;
        xFechaHoraInicio: DateTime;
        xCodAusencia: Code[10];
        xDescAusencia: Code[10];
        xFechaHoraFin: DateTime;
        xLimpiar: Boolean;

        cugVblesGlobalesPre: Codeunit TCN_VblesGlobalesPre;
        cugFuncionesPresencia: Codeunit TCN_FuncionesPresencia;

        // [EventSubscriber(ObjectType::Page, Page::TCN_CuesPresenscia, 'OnPushEntradaF', '', false, false)]
        // local procedure MyProcedure()
        // begin

        // end;

    trigger OnAfterGetCurrRecord()
    begin
        cugVblesGlobalesPre.GetPasaPArametrosF(xEmpleado, xFechaHoraInicio, xCodAusencia, xLimpiar);
        if xLimpiar then begin
            ClearAll();
        end;
    end;

    local procedure PasarDatosGlobalF()
    var
    begin
        // cugVblesGlobalesPre.SetPasaPArametrosF(xEmpleado, xFechaHoraInicio);

        cugVblesGlobalesPre.SetPasaPArametrosF(xempleado, xFechaHoraInicio, xCodAusencia, false);
        CurrPage.Update(false);
    end;
}