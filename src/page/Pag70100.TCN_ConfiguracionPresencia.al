page 70100 "TCN_ConfiguracionPresencia"
{
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = "TCN_TablaConfPresencia";
    Caption = 'Pagina de Presencia';
    InsertAllowed = false;
    DeleteAllowed = false;
    //DelayedInsert = false;          // cnd cambian de campo hace el insert, no retrasa la insecion
    //PopulateAllFields = true;      // rellena los campos de mi tabla automaticamente. True los rellena
    // //AutoSplitKey = true;             nos pone la liena su codigo automaticamente. Si se añade entre medias calcula el codigo
    //                                     ej: linea 10000 linea 20000, seria la 15000

    layout
    {
        area(Content)
        {
            group(General)
            {
                // lo quitamos para que el usuario no pueda acceder a el
                // field(ID; ID)
                // {
                //     ApplicationArea = All;

                // }
                field("Minutos de cortesia entrada"; "Minutos de cortesia entrada")
                {
                    ApplicationArea = All;
                }
                field("Minutos de cortesia salida"; "Minutos de cortesia salida")
                {
                    ApplicationArea = All;
                }
                field("Minutos entre fichajes"; "Minutos entre fichajes")
                {
                    ApplicationArea = All;
                }
                /* Dias Semana */

            }
            group(DiasSemana)
            {
                Caption = 'Días Semana';
                field(Lunes; Lunes)
                {
                    ApplicationArea = All;
                }
                field(Martes; Martes)
                {
                    ApplicationArea = All;
                }
                field(Miercoles; Miercoles)
                {
                    ApplicationArea = All;
                }
                field(Jueves; Jueves)
                {
                    ApplicationArea = All;
                }
                field(Viernes; Viernes)
                {
                    ApplicationArea = All;
                }
                field(Sabado; Sabado)
                {
                    ApplicationArea = All;
                }
                field(Domingo; Domingo)
                {
                    ApplicationArea = All;
                }
            }
        }
    }
    trigger OnOpenPage()
    var
        clFuncionesPresencia: Codeunit TCN_FuncionesPresencia;
    begin
        getF();
        CurrPage.Editable(clFuncionesPresencia.EsSuperUPreF());

    end;


}