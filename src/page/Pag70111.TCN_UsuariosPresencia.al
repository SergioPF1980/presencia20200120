page 70111 "TCN_UsuariosPresencia"
{

    PageType = List;
    SourceTable = TCN_UsuariosPre;
    Caption = 'Usuarios Presencia';
    ApplicationArea = All;
    UsageCategory = Administration;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(IdUsuario; IdUsuario)
                {
                    ApplicationArea = All;
                }

                field(NombreUsuario; NombreUsuarioF(IdUsuario))
                {
                    Caption = 'Nombre Usuario';
                }
                field(SuperPresencia; SuperPresencia)
                {
                    ApplicationArea = All;
                }
            }
        }
    }


    //  Al entrar en Usuarios Presencia compruebo si es SuperUsuario, si no eres sale un error 
    trigger OnOpenPage()
    var
        clFuncionesPresencia: Codeunit TCN_FuncionesPresencia;
        xlEror: Label 'no eres super usuario';
    begin
        if not clFuncionesPresencia.EsSuperUPreF() then begin
            Error(xlEror);
        end;
    end;
}
